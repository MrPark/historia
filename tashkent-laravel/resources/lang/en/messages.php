<?php

return [
    'gallery' => 'Gallery',
    'search' => 'Search',
    'architecture' => 'Architecture',
    'individuals' => 'Individuals',
    'events' => 'Events',
    'previous' => 'Previous',
    'next_page' => 'Next Page',
    'about' => 'About us',
    'links' => 'Links',
    'our_team' => 'Our team',
    'mrpark_description' => 'Programmer. Lomonosov MSU Tashkent branch student.',
    'about_project' => 'About the project',
    'about_quote' => 'The best benefit we derive from history is the enthusiasm it excites.',
    'Goethe' => 'Johann Wolfgang von Goethe',
    'about_text' => 'The project considers the history of Tashkent in the period from 1865 to 2017.',
];
