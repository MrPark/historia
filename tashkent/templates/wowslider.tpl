{{define "wowSlider"}}

<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
<link rel="stylesheet" type="text/css" href="/static/engine0/style.css" />
<script type="text/javascript" src="/static/engine0/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
<div id="wowslider-container0">
<div class="ws_images"><ul>
    <li><img src="/static/data0/images/tashkent25dreamblur.jpg" alt="tashkent-25dreamblur" title="tashkent-25dreamblur" id="wows0_0"/></li>
    <li><a href="#"><img src="/static/data0/images/_0.jpg" alt="wowslider" title="Абракадабра" id="wows0_1"/></a></li>
    <li><img src="/static/data0/images/__.jpg" alt="Военный Спасо-Преображенский Собор" title="Военный Спасо-Преображенский Собор" id="wows0_2"/></li>
    <li><img src="/static/data0/images/555f095e9c230dreamblur.jpg830x360.jpg" alt="Военный Спасо-Преображенский Собор" title="Дворец Форумов" id="wows0_3"/></li>
</ul></div>
<div class="ws_bullets"><div>
    <a href="#" title="tashkent-25dreamblur"><span><img src="/static/data0/tooltips/tashkent25dreamblur.jpg" alt="tashkent-25dreamblur"/>1</span></a>
    <a href="#" title="Абракадабра"><span><img src="/static/data0/tooltips/_0.jpg" alt="Абракадабра"/>2</span></a>
    <a href="#" title="Военный Спасо-Преображенский Собор"><span><img src="/static/data0/tooltips/__.jpg" alt="Военный Спасо-Преображенский Собор"/>3</span></a>
    <a href="#" title="Военный Спасо-Преображенский Собор"><span><img src="/static/data0/tooltips/555f095e9c230dreamblur.jpg110x48.jpg" alt="Военный Спасо-Преображенский Собор"/>4</span></a>
</div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="#">slider</a> by WOWSlider.com v8.7</div>
<div class="ws_shadow"></div>
</div>  
<script type="text/javascript" src="/static/engine0/wowslider.js"></script>
<script type="text/javascript" src="/static/engine0/script.js"></script>
<!-- End WOWSlider.com BODY section -->

</html>

{{end}}